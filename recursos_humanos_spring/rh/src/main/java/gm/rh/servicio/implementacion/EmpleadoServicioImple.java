package gm.rh.servicio.implementacion;

import gm.rh.modelo.Empleado;
import gm.rh.repositorio.EmpleadoRepo;
import gm.rh.servicio.EmpleadoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmpleadoServicioImple implements EmpleadoServicio {

    @Autowired
    private EmpleadoRepo empleadoRepo;

    @Override
    @Transactional(readOnly = true)
    public List<Empleado> listarEmpleado() {
        return empleadoRepo.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Empleado buscarEmpleadoPorId(Integer idEmpleado) {
        return empleadoRepo.findById(idEmpleado).orElse(null);
    }

    @Override
    public Empleado guardarEmpleado(Empleado empleado) {
        return empleadoRepo.save(empleado);
    }

    @Override
    public void eliminarEmpleado(Empleado empleado) {
        empleadoRepo.delete(empleado);
    }
}

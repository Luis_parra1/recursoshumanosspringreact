package gm.rh.Excepciones;

public class RecursoNoEncontradoExcepcion extends RuntimeException{
    public RecursoNoEncontradoExcepcion(String message) {
        super(message);
    }
}
